package ru.teterin.tm.service;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.error.*;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.*;

public class TaskService {

        private final TaskRepository taskRepository;

        private final ProjectRepository projectRepository;

        public TaskService(TaskRepository taskRepository, ProjectRepository projectRepository) {
                this.taskRepository = taskRepository;
                this.projectRepository = projectRepository;
        }

        public Task findOne(String userId, String id) {
                Task task;
                if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                task = taskRepository.findOne(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
                if (task == null) {
                        throw new ObjectNotFoundException();
                }
                return task;
        }

        public Collection<Task> findAll(String userId) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                return taskRepository.findAll(DateUuidParseUtil.toUUID(userId));
        }

        // Добавление новой задачи
        public Task persist(String userId, Task task) throws ObjectExistException {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                if (task == null || task.getName().isEmpty() || task.getUserId() == null) {
                        throw new IncorrectObject("Задача введена некоректно!");
                }
                if (!task.getUserId().equals(userId)) {
                        throw new IncorrectObject("Эта задача другого пользователя!");
                }
                return taskRepository.persist(DateUuidParseUtil.toUUID(userId), task);
        }

        //Добавление/изменение задачи
        public Task merge(String userId, Task task) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                if (task == null || task.getName().isEmpty() || task.getUserId() == null) {
                        throw new IncorrectObject("Задача введена некоректно!");
                }
                if (!task.getUserId().equals(userId)) {
                        throw new IncorrectObject("Эта задача другого пользователя!");
                }
                return taskRepository.merge(DateUuidParseUtil.toUUID(userId), task);
        }

        public Task remove(String userId, String id) {
                Task task;
                if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                id = DateUuidParseUtil.toUUID(id);
                userId = DateUuidParseUtil.toUUID(userId);
                return taskRepository.remove(userId, id);
        }

        public void removeAll(String userId) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                taskRepository.removeAll(DateUuidParseUtil.toUUID(userId));
        }

        public Task createTask(String userId, String projectId, String name, String description, String startDate, String endDate) {
                boolean noName = name == null || name.isEmpty();
                boolean incorrectID = projectId == null || projectId.isEmpty() || userId == null || userId.isEmpty();
                boolean noStartDate = startDate == null || startDate.isEmpty();
                boolean noEndDate = endDate == null || endDate.isEmpty();
                if (noName) {
                        throw new IncorrectObject("Задача введена некоректно!");
                }
                if (incorrectID) {
                        throw new IncorrectUuidException();
                }
                if (description == null) {
                        throw new IncorrectObject("Задача введена некоректно!");
                }
                if (noStartDate || noEndDate) {
                        throw new IncorrectDateException();
                }
                return new Task(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(projectId), name, description, DateUuidParseUtil.toDate(startDate), DateUuidParseUtil.toDate(endDate));
        }

        public Task linkTask(String userId, String projectId, String id) {
                boolean noProjectId = projectId == null || projectId.isEmpty();
                boolean noTaskId = id == null || id.isEmpty();
                boolean noUserId = userId == null || userId.isEmpty();
                Project project;
                Task task;
                if (noProjectId || noTaskId || noUserId) {
                        throw new IncorrectUuidException();
                }
                userId = DateUuidParseUtil.toUUID(userId);
                projectId = DateUuidParseUtil.toUUID(projectId);
                id = DateUuidParseUtil.toUUID(id);
                project = projectRepository.findOne(userId, projectId);
                task = findOne(userId, id);
                if (project == null || task == null) {
                        throw new ObjectNotFoundException();
                }
                task.setProjectId(projectId);
                return task;
        }

}
