package ru.teterin.tm.error;

public class IncorrectObject extends RuntimeException {

        @Override
        public void printStackTrace() {
                System.out.println("Объект введен некоректно!");
        }

        public IncorrectObject(String message) {
                super(message);
                System.out.println(message);
        }

}
