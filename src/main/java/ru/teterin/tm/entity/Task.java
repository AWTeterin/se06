package ru.teterin.tm.entity;

import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Date;
import java.util.UUID;

public class Task {

        private String id = UUID.randomUUID().toString();

        private String projectId;

        private String userId;

        private String name = "";

        private String description = "";

        private Date dateStart = new Date();

        private Date dateEnd = new Date();

        public Task(String name, String id) {
                this.name = name;
                this.id = id;
        }

        public Task(String userId, String projectId, String name, String description, Date dateStart, Date dateEnd) {
                this.name = name;
                this.projectId = projectId;
                this.userId = userId;
                this.description = description;
                this.dateStart = dateStart;
                this.dateEnd = dateEnd;
        }

        public String getName() {
                return this.name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getUserId() {
                return userId;
        }

        public void setUserId(String userId) {
                this.userId = userId;
        }

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public String getProjectId() {
                return projectId;
        }

        public void setProjectId(String projectId) {
                this.projectId = projectId;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public Date getDateStart() {
                return dateStart;
        }

        public void setDateStart(Date dateStart) {
                this.dateStart = dateStart;
        }

        public Date getDateEnd() {
                return dateEnd;
        }

        public void setDateEnd(Date dateEnd) {
                this.dateEnd = dateEnd;
        }

        @Override
        public String toString() {
                return "Task{" +
                        "id='" + id + '\'' +
                        ", projectId='" + projectId + '\'' +
                        ", name='" + name + '\'' +
                        ", description='" + description + '\'' +
                        ", dateStart=" + DateUuidParseUtil.DATE_FORMAT.format(dateStart) +
                        ", dateEnd=" + DateUuidParseUtil.DATE_FORMAT.format(dateEnd) +
                        '}';
        }

}
