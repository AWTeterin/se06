package ru.teterin.tm.command.task;

import ru.teterin.tm.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "task-remove";
        }

        @Override
        public String getDescription() {
                return "Remove task by ID.";
        }

        @Override
        public void execute() {
                System.out.println("[TASK REMOVE]");
                System.out.println("ENTER ID");
                bootstrap.getTaskService().remove(bootstrap.getStateService().getUserId(), scanner.nextLine());
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
