package ru.teterin.tm.command.task;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Task;

public class TaskEditCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "task-edit";
        }

        @Override
        public String getDescription() {
                return "Choose task by id and edit it.";
        }

        @Override
        public void execute() {
                String userId = bootstrap.getStateService().getUserId();
                String id;
                String name;
                String description;
                String startDate;
                String endDate;
                Task task;
                Task result;
                System.out.println("[TASK EDIT]");
                System.out.println("ENTER ID:");
                id = scanner.nextLine();
                task = bootstrap.getTaskService().findOne(userId, id);
                System.out.println(task);
                System.out.println("ENTER NAME:");
                name = scanner.nextLine();
                System.out.println("ENTER DESCRIPTION:");
                description = scanner.nextLine();
                System.out.println("ENTER START DATE:");
                startDate = scanner.nextLine();
                System.out.println("ENTER END DATE:");
                endDate = scanner.nextLine();
                result = bootstrap.getTaskService().createTask(userId, name, task.getProjectId(), description, startDate, endDate);
                result.setId(task.getId());
                bootstrap.getTaskService().merge(userId, result);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
