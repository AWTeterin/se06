package ru.teterin.tm.command.user;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.User;

public class UserLogInCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "user-login";
        }

        @Override
        public String getDescription() {
                return "Log in to the task manager.";
        }

        @Override
        public void execute() {
                String login;
                String password;
                User user;
                System.out.println("[USER LOGIN]");
                System.out.println("ENTER LOGIN:");
                login = scanner.nextLine();
                System.out.println("ENTER PASSWORD:");
                password = scanner.nextLine();
                user = bootstrap.getUserService().checkUser(login, password);
                bootstrap.getStateService().setUserId(user.getId());
                bootstrap.getStateService().setLogin(user.getLogin());
                bootstrap.getStateService().setRole(user.getRole());
                System.out.println("HELLO " + user.getLogin());
        }

        @Override
        public boolean secure() {
                return false;
        }

}
