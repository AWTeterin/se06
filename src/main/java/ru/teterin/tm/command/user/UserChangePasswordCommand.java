package ru.teterin.tm.command.user;

import ru.teterin.tm.command.AbstractCommand;

public class UserChangePasswordCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "user-password";
        }

        @Override
        public String getDescription() {
                return "Change user password.";
        }

        @Override
        public void execute() {
                String oldPassword;
                String newPassword;
                System.out.println("[CHANGE PASSWORD]");
                System.out.println("ENTER OLD PASSWORD:");
                oldPassword = scanner.nextLine();
                System.out.println("ENTER NEW PASSWORD:");
                newPassword = scanner.nextLine();
                bootstrap.getUserService().changePassword(bootstrap.getStateService().getLogin(), oldPassword, newPassword);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
