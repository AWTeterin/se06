package ru.teterin.tm.command.user;

import ru.teterin.tm.command.AbstractCommand;

public class UserShowCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "user-show";
        }

        @Override
        public String getDescription() {
                return "Show user data.";
        }

        @Override
        public void execute() {
                System.out.println("USER: " + bootstrap.getStateService().getLogin());
                System.out.println("ROLE: " + bootstrap.getStateService().getRole().displayName());
        }

        @Override
        public boolean secure() {
                return true;
        }

}
