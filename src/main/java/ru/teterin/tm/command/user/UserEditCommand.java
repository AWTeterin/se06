package ru.teterin.tm.command.user;

import ru.teterin.tm.command.AbstractCommand;

public class UserEditCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "user-edit";
        }

        @Override
        public String getDescription() {
                return "Edit user login.";
        }

        @Override
        public void execute() {
                String oldLogin = bootstrap.getStateService().getLogin();
                String newLogin;
                System.out.println("[USER EDIT]");
                System.out.println("ENTER NEW LOGIN:");
                newLogin = scanner.nextLine();
                bootstrap.getUserService().editUser(oldLogin, newLogin);
                bootstrap.getStateService().setLogin(newLogin);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
