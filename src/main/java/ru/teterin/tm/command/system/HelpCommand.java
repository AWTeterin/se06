package ru.teterin.tm.command.system;

import ru.teterin.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "help";
        }

        @Override
        public String getDescription() {
                return "Show all commands.";
        }

        @Override
        public void execute() {
                for (final AbstractCommand command : bootstrap.getCommands()) {
                        System.out.println(command.getName() + ": " + command.getDescription());
                }
        }

        @Override
        public boolean secure() {
                return false;
        }

}
