package ru.teterin.tm.command;

import ru.teterin.tm.context.Bootstrap;
import ru.teterin.tm.enumerated.Role;

import java.util.Scanner;

public abstract class AbstractCommand {

        protected final Scanner scanner = new Scanner(System.in);

        protected Bootstrap bootstrap;

        public void setBootstrap(Bootstrap bootstrap) {
                this.bootstrap = bootstrap;
        }

        public Bootstrap getBootstrap() {
                return bootstrap;
        }

        public abstract String getName();

        public abstract String getDescription();

        public abstract void execute();

        public abstract boolean secure();

        public Role[] roles() {
                return null;
        }

}
