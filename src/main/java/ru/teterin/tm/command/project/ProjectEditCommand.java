package ru.teterin.tm.command.project;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Project;

public class ProjectEditCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "project-edit";
        }

        @Override
        public String getDescription() {
                return "Choose project by id and edit it.";
        }

        @Override
        public void execute() {
                String id;
                String userId = bootstrap.getStateService().getUserId();
                String name;
                String description;
                String startDate;
                String endDate;
                Project project;
                Project result;
                System.out.println("[PROJECT EDIT]");
                System.out.println("ENTER ID:");
                id = scanner.nextLine();
                project = bootstrap.getProjectService().findOne(userId, id);
                System.out.println(project);
                System.out.println("ENTER NAME:");
                name = scanner.nextLine();
                System.out.println("ENTER DESCRIPTION:");
                description = scanner.nextLine();
                System.out.println("ENTER START DATE:");
                startDate = scanner.nextLine();
                System.out.println("ENTER END DATE:");
                endDate = scanner.nextLine();
                result = bootstrap.getProjectService().createProject(userId, name, description, startDate, endDate);
                result.setId(project.getId());
                bootstrap.getProjectService().merge(userId, result);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
