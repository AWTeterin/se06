package ru.teterin.tm.command.project;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.util.DateUuidParseUtil;

public class ProjectTasksCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "project-tasks";
        }

        @Override
        public String getDescription() {
                return "Show all tasks for this project.";
        }

        @Override
        public void execute() {
                System.out.println("[PROJECT TASKS]");
                System.out.println("ENTER PROJECT ID");
                DateUuidParseUtil.printCollection(bootstrap.getProjectService().findAllTaskByProjectId(bootstrap.getStateService().getUserId(), scanner.nextLine()));
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
