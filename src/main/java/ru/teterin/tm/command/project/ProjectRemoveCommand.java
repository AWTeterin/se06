package ru.teterin.tm.command.project;

import ru.teterin.tm.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "project-remove";
        }

        @Override
        public String getDescription() {
                return "Remove project by ID.";
        }

        @Override
        public void execute() {
                System.out.println("[PROJECT REMOVE]");
                System.out.println("ENTER ID:");
                bootstrap.getProjectService().remove(bootstrap.getStateService().getUserId(), scanner.nextLine());
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
