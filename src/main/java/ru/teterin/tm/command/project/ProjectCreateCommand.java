package ru.teterin.tm.command.project;

import ru.teterin.tm.command.AbstractCommand;

public class ProjectCreateCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "project-create";
        }

        @Override
        public String getDescription() {
                return "Create new Project.";
        }

        @Override
        public void execute() {
                String userId = bootstrap.getStateService().getUserId();
                String name;
                String description;
                String startDate;
                String endDate;
                System.out.println("[PROJECT CREATE]");
                System.out.println("ENTER NAME:");
                name = scanner.nextLine();
                System.out.println("ENTER DESCRIPTION:");
                description = scanner.nextLine();
                System.out.println("ENTER START DATE:");
                startDate = scanner.nextLine();
                System.out.println("ENTER END DATE:");
                endDate = scanner.nextLine();
                bootstrap.getProjectService().persist(userId, bootstrap.getProjectService().createProject(userId, name, description, startDate, endDate));
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
