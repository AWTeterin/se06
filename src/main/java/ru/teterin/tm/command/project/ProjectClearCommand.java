package ru.teterin.tm.command.project;

import ru.teterin.tm.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "project-clear";
        }

        @Override
        public String getDescription() {
                return "Remove all project.";
        }

        @Override
        public void execute() {
                System.out.println("[PROJECTS CLEAR]");
                bootstrap.getProjectService().removeAll(bootstrap.getStateService().getUserId());
                System.out.println("[ALL PROJECTS REMOVED]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
