package ru.teterin.tm.command.project;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.util.DateUuidParseUtil;

public class ProjectListCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "project-list";
        }

        @Override
        public String getDescription() {
                return "Show all project.";
        }

        @Override
        public void execute() {
                System.out.println("[PROJECT LIST]");
                DateUuidParseUtil.printCollection(bootstrap.getProjectService().findAll(bootstrap.getStateService().getUserId()));
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
