package ru.teterin.tm.repository;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.error.ObjectExistException;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> {

        private final Map<String, Project> projectMap = new LinkedHashMap<>();

        @Override
        public Collection<Project> findAll(String userId) {
                Collection<Project> result = new LinkedHashSet<>();
                for (Project project : projectMap.values()) {
                        if (project.getUserId().equals(userId)) {
                                result.add(project);
                        }
                }
                return result;
        }

        @Override
        public Project findOne(String userId, String id) {
                Project project = projectMap.get(id);
                if (project == null || !project.getUserId().equals(userId)) {
                        return null;
                }
                return project;
        }

        //Добавление нового проекта в репозиторий, если нет выбрасываем исключение
        @Override
        public Project persist(String userId, Project newObject) throws ObjectExistException {
                if (findOne(userId, newObject.getId()) != null) {
                        throw new ObjectExistException();
                }
                return projectMap.put(newObject.getId(), newObject);
        }

        //Добавление нового проекта если его нет в репозитории, иначе замена старого
        @Override
        public Project merge(String userId, Project object) {
                return projectMap.put(object.getId(), object);
        }

        @Override
        public Project remove(String userId, String id) {
                Project project = findOne(userId, id);
                if (project == null) {
                        return null;
                }
                return projectMap.remove(id);
        }

        @Override
        public void removeAll(String userId) {
                Collection<Project> projects = findAll(userId);
                for (Project project : projects) {
                        projectMap.remove(project.getId());
                }
        }

}
