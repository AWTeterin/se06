package ru.teterin.tm.repository;

import ru.teterin.tm.entity.User;
import ru.teterin.tm.error.ObjectExistException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> {

        private Map<String, User> userMap = new LinkedHashMap<>();

        @Override
        public Collection<User> findAll(String userId) {
                return userMap.values();
        }

        @Override
        public User findOne(String userId, String id) {
                return userMap.get(id);
        }

        @Override
        public User persist(String userId, User newObject) throws ObjectExistException {
                if (!userMap.containsKey(newObject.getId())) {
                        return userMap.put(newObject.getId(), newObject);
                }
                throw new ObjectExistException();
        }

        @Override
        public User merge(String userId, User object) {
                return userMap.put(object.getId(), object);
        }

        @Override
        public User remove(String userId, String id) {
                return userMap.remove(id);
        }

        @Override
        public void removeAll(String userId) {
                userMap.clear();
        }

        public User findByLogin(String login) {
                Collection<User> users = userMap.values();
                for (User user : users) {
                        if (user.getLogin().equals(login)) {
                                return user;
                        }
                }
                return null;
        }

}
