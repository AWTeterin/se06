package ru.teterin.tm.repository;

import ru.teterin.tm.entity.Task;
import ru.teterin.tm.error.ObjectExistException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class TaskRepository extends AbstractRepository<Task> {

        private final Map<String, Task> taskMap = new LinkedHashMap<>();

        @Override
        public Collection<Task> findAll(String userId) {
                Collection<Task> result = new LinkedHashSet<>();
                for (Task task : taskMap.values()) {
                        if (task.getUserId().equals(userId)) {
                                result.add(task);
                        }
                }
                return result;
        }

        @Override
        public Task findOne(String userId, String id) {
                Task task = taskMap.get(id);
                if (task == null || !task.getUserId().equals(userId)) {
                        return null;
                }
                return task;
        }

        //Добавление новой задачи если ее нет в репозитории, иначе выбрасываем исключение
        @Override
        public Task persist(String userId, Task newObject) throws ObjectExistException {
                if (findOne(userId, newObject.getId()) != null) {
                        throw new ObjectExistException();
                }
                return taskMap.put(newObject.getId(), newObject);
        }

        //Добавление новой задачи если ее нет в репозитории, иначе замена старого
        @Override
        public Task merge(String userId, Task object) {
                return taskMap.put(object.getId(), object);
        }

        @Override
        public Task remove(String userId, String id) {
                Task task = findOne(userId, id);
                if (task == null) {
                        return null;
                }
                return taskMap.remove(id);
        }

        @Override
        public void removeAll(String userId) {
                Collection<Task> tasks = findAll(userId);
                for (Task task : tasks) {
                        taskMap.remove(task.getId());
                }
        }

        public void removeAllTaskByProjectId(String userId, String id) {
                Collection<Task> list = findAll(userId);
                list.removeIf(x -> x.getProjectId().equals(id));
        }

        public Collection<Task> findAllTaskByProjectId(String userId, String projectId) {
                Collection<Task> result = new LinkedHashSet<>();
                for (Task task : findAll(userId)) {
                        if (task.getProjectId().equals(projectId)) {
                                result.add(task);
                        }
                }
                return result;
        }

}
