package ru.teterin.tm;

import ru.teterin.tm.context.Bootstrap;

public class Application {

        public static void main(String[] args) {
                new Bootstrap().init();
        }

}
