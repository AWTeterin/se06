package ru.teterin.tm.context;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.command.project.*;
import ru.teterin.tm.command.system.*;
import ru.teterin.tm.command.task.*;
import ru.teterin.tm.command.user.*;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.error.*;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.service.ProjectService;
import ru.teterin.tm.service.StateService;
import ru.teterin.tm.service.TaskService;
import ru.teterin.tm.service.UserService;
import ru.teterin.tm.util.HashUtil;

import java.util.*;

public class Bootstrap {

        private final TaskRepository taskRepository = new TaskRepository();

        private final ProjectRepository projectRepository = new ProjectRepository();

        private final UserRepository userRepository = new UserRepository();

        private final TaskService taskService = new TaskService(taskRepository, projectRepository);

        private final ProjectService projectService = new ProjectService(projectRepository, taskRepository);

        private final UserService userService = new UserService(userRepository);

        private final StateService stateService = new StateService();

        private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

        private void registry(AbstractCommand command) {
                command.setBootstrap(this);
                commands.put(command.getName(), command);
        }

        public void init() {
                usersInit();
                commandsInit();
                System.out.println("*** WELCOME TO TASK MANAGER ***");
                while (true) {
                        final String cmd = new Scanner(System.in).nextLine();
                        final AbstractCommand command = this.commands.get(cmd);
                        try {
                                if (command == null || (command.secure() && stateService.getRole() == null)) {
                                        System.out.println("THE COMMAND IS NOT AVAILABLE!");
                                        continue;
                                }
                                command.execute();
                        } catch (ObjectExistException e) {
                                System.out.println("OBJECT ALREADY EXIST !");
                        } catch (IncorrectUuidException e) {
                                System.out.println("INCORRECT ID FORMAT: USE FORMAT 00000000-0000-0000-0000-000000000000 !");
                        } catch (ObjectNotFoundException e) {
                                System.out.println("OBJECT NOT FOUND !");
                        } catch (IncorrectDateException e) {
                                System.out.println("INCORRECT DATE FORMAT: USE FORMAT 01.01.2020 !");
                        } catch (IncorrectObject e) {
                                System.out.println("OBJECT OR OBJECT NAME MUST NOT BE EMPTY !");
                        }
                }
        }

        public Collection<AbstractCommand> getCommands() {
                return commands.values();
        }

        public TaskService getTaskService() {
                return taskService;
        }

        public ProjectService getProjectService() {
                return projectService;
        }

        public UserService getUserService() {
                return userService;
        }

        public StateService getStateService() {
                return stateService;
        }

        public void usersInit() {
                User user = new User("user", HashUtil.md5Custom("user"), Role.USER);
                User admin = new User("admin", HashUtil.md5Custom("admin"), Role.ADMIN);
                userRepository.persist(user.getId(), user);
                userRepository.persist(admin.getId(), admin);
        }

        public void commandsInit() {
                registry(new HelpCommand());
                registry(new ProjectListCommand());
                registry(new ProjectTasksCommand());
                registry(new ProjectCreateCommand());
                registry(new ProjectEditCommand());
                registry(new ProjectRemoveCommand());
                registry(new ProjectClearCommand());
                registry(new TaskListCommand());
                registry(new TaskCreateCommand());
                registry(new TaskEditCommand());
                registry(new TaskLinkCommand());
                registry(new TaskRemoveCommand());
                registry(new TaskClearCommand());
                registry(new ExitCommand());
                registry(new UserCreateCommand());
                registry(new UserEditCommand());
                registry(new UserChangePasswordCommand());
                registry(new UserLogInCommand());
                registry(new UserShowCommand());
                registry(new UserLogOutCommand());
        }

}
